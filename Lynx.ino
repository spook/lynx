#include <SBUS.h>

#define SbusSerial Serial2

// Front right
#define EngineFRIn1 3
#define EngineFRIn2 4

// Back right
#define EngineBRIn1 7
#define EngineBRIn2 8

// Front left
#define EngineFLIn1 35
#define EngineFLIn2 36

// Back left
#define EngineBLIn1 37
#define EngineBLIn2 38

#define Servo1Pwm 20
#define Servo2Pwm 21
#define Servo3Pwm 22
#define Servo4Pwm 23

#define DistanceMeterTrig 24
#define DistanceMeterEcho 25

#define ForwardChannel 0
#define SideChannel 1
#define RotChannel 2
#define CameraPanChannel 3
#define CameraTiltChannel 4

SBUS rx(Serial2);
uint16_t channels[16];
bool failSafe;
bool lostFrame;

const int rxPwmRange = 2048;
const int rxPwmRangeHalf = rxPwmRange / 2;

// Servo PWM frequency = 50 Hz : one cycle = 20ms
// Min servo value = 0.5ms = 1/40 of 20ms, with 10 bit precision = 25.6
const int servoPwmMin = 26;
// Max servo value = 2.5ms = 1/8 of 20ms, with 10 bit precision = 128
// Fixed to actual servo capabilities
const int servoPwmMax = 120;
const int servoPwmRange = servoPwmMax - servoPwmMin;

const int enginePwmRange = 1024;

const int servoMaxSpeedPwmPerSec = 4096; // 180 deg in 0.5s

int currentPanX1000 = (servoPwmMin + servoPwmMax) / 2 * 1000;
int currentTiltX1000 = (servoPwmMin + servoPwmMax) / 2 * 1000;

//                                   FL FR BL BR
const int FrontEngineActions[4] =      { 1, 1, 1, 1 };
const int RotRightEngineActions[4] =   { 1,-1, 1,-1 };
const int ShiftRightEngineActions[4] = { 1,-1,-1, 1 };

void moveEngine(int32_t value, int pin1, int pin2)
{
  if (value > 0) {
    digitalWrite(pin1, 0);
    analogWrite(pin2, value);
  } else {
    digitalWrite(pin2, 0);
    analogWrite(pin1, -value);    
  }
}

void handleServos(uint16_t * channels) {

  uint16_t pwm = (rxPwmRange - channels[2]) * servoPwmRange / rxPwmRange + servoPwmMin;
  analogWrite(21, pwm);

  pwm = (rxPwmRange - channels[3]) * servoPwmRange / rxPwmRange + servoPwmMin;
  analogWrite(20, pwm);
}

void handleEngines(uint16_t * channels) {

  float fwd = (channels[ForwardChannel] - rxPwmRangeHalf) / (rxPwmRangeHalf * 1.0f);
  float side = (channels[SideChannel] - rxPwmRangeHalf) / (rxPwmRangeHalf * 1.0f); 
  float rot = (channels[RotChannel] - rxPwmRangeHalf) / (rxPwmRangeHalf * 1.0f); 
  float sum = abs(fwd) + abs(side) + abs(rot);

  int32_t FLEngine = 0, FREngine = 0, BLEngine = 0, BREngine = 0;

  if (sum > 0.0001f)
  {
    FLEngine = (int)((FrontEngineActions[0] * fwd * (abs(fwd) / sum) + RotRightEngineActions[0] * rot * (abs(rot) / sum) + ShiftRightEngineActions[0] * side * (abs(side) / sum)) * enginePwmRange);
    FREngine = (int)((FrontEngineActions[1] * fwd * (abs(fwd) / sum) + RotRightEngineActions[1] * rot * (abs(rot) / sum) + ShiftRightEngineActions[1] * side * (abs(side) / sum)) * enginePwmRange);
    BLEngine = (int)((FrontEngineActions[2] * fwd * (abs(fwd) / sum) + RotRightEngineActions[2] * rot * (abs(rot) / sum) + ShiftRightEngineActions[2] * side * (abs(side) / sum)) * enginePwmRange);
    BREngine = (int)((FrontEngineActions[3] * fwd * (abs(fwd) / sum) + RotRightEngineActions[3] * rot * (abs(rot) / sum) + ShiftRightEngineActions[3] * side * (abs(side) / sum)) * enginePwmRange);
  }

  // Front left engine
  moveEngine(FLEngine, EngineFLIn2, EngineFLIn1);
  // Back left engine
  moveEngine(BLEngine, EngineBLIn1, EngineBLIn2);
  // Front right engine
  moveEngine(FREngine, EngineFRIn1, EngineFRIn2);
  // Back right engine
  moveEngine(BREngine, EngineBRIn2, EngineBRIn1);
}

void setup() {
  // put your setup code here, to run once:

  rx.begin();
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(35, OUTPUT);
  pinMode(36, OUTPUT);
  pinMode(37, OUTPUT);
  pinMode(38, OUTPUT);

  analogWriteFrequency(21, 50);
  analogWriteResolution(10);

  analogWriteFrequency(3, 20000);
  analogWriteFrequency(7, 20000);
}

void loop() {
  // put your main code here, to run repeatedly:

  if (rx.read(&channels[0], &failSafe, &lostFrame)) {

    // handleServos(channels);
    handleEngines(channels);   
  }
}
